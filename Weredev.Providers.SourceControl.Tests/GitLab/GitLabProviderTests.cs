using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Weredev.Providers.SourceControl.GitLab;

namespace Weredev.Providers.SourceControl.Tests.GitLab
{
    public class GitLabProviderTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task GetReadmeMarkdown_WhenAvailable_ReturnsDecodedString()
        {
            var provider = new GitLabProvider();
            var content = await provider.GetReadmeMarkdown("thornless");
            Assert.NotNull(content);
            Assert.IsTrue(content.Contains('*'));
        }
    }
}
