using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Weredev.Providers.SourceControl.GitHub;

namespace Weredev.Providers.SourceControl.Tests.GitHub
{
    public class GitHubProviderTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task GetReadmeMarkdown_WhenMissing_ReturnsNull()
        {
            var provider = new GitHubProvider();
            var content = await provider.GetReadmeMarkdown(Guid.NewGuid().ToString());
            Assert.IsNull(content);
        }

        [Test]
        public async Task GetReadmeMarkdown_WhenAvailable_ReturnsDecodedString()
        {
            var provider = new GitHubProvider();
            var content = await provider.GetReadmeMarkdown("thornless");
            Assert.NotNull(content);
            Assert.IsTrue(content.Contains('*'));
        }

        [Test]
        public async Task GetReadmeMarkdown_WhenHasRelativeUrl_InflatesUrl()
        {
            var provider = new GitHubProvider();
            var content = await provider.GetReadmeMarkdown("vscode-copyrawvalue");
            Assert.NotNull(content);
            Assert.IsTrue(content.Contains(@"https://raw.githubusercontent.com/WereDev/vscode-copyrawvalue/master/resources/demo.gif?raw=true"));
        }
    }
}
