# weredev.com
Code for my main website.  You're viewing it right now.

## Technology
This is a pretty straight forward build using .Net Core.  The code is broken down into 3 basic tiers: UI, Domain (logic), and Repo/Providers.  I'm also using StyleCop for general code analysis and keeping stuff tidy.

### UI
The UI is built using ASP.Net with the MVC pattern.  The built in pipeline and dependency injection framework is used with navagation based on controller and action attributes, as opposed to Razor pages.

The photo album uses PhotoSwipe (https://photoswipe.com/) to provide the light box experience.

### Domain
This layer has basic logic and mapping, mostly just the mapping.  Caching is also implemented at this layer to cache responses from the Repo/Provider layer.

### Repo / Provider
Both repos use HttpClients. The two biggest parts of this are pulling data from Flickr and from GitHub/GitLab.  The Flickr provider was interesting as the way the Flickr organises photos is different from how I wanted to display them on this site.  Caching from Flickr is done in the file system to provide long term cache since its only a few times a year at most that new photos get added.  The service for retrieving from GitHub and GitLab uses in-memory cache to avoid spamming those services with requests but still need to be udpatable more freqently.

## Deployment
Deployments are being handle with GitLab pipelines.  This is triggered automatically my checking in to the master branch.

## Dependencies
[AutoMapper](https://automapper.org)

[MarkdownSharp](https://github.com/esskar/MarkdownSharp)

[StyleCop](https://github.com/StyleCop/StyleCop)

[PhotoSwipe](https://photoswipe.com/)
