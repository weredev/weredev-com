using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Weredev.Providers.Flickr.Tests
{
    [TestFixture]
    public class FlickrProviderTests
    {
        [Test]
        public async Task GetNavigationList_Success()
        {
            using var provider = GetFlickrProvider();
            var navList = await provider.ListCollections();
            Assert.IsNotNull(navList);
            Assert.Greater(navList.Length, 0);
            foreach (var navItem in navList)
            {
                Assert.IsFalse(string.IsNullOrWhiteSpace(navItem.Id));
                Assert.IsFalse(string.IsNullOrWhiteSpace(navItem.Title));
            }
        }

        [Test]
        public async Task GetPhotosetPhotos()
        {
            using var provider = GetFlickrProvider();
            var photos = await provider.ListPhotos("72157675450153882");
            Assert.IsNotNull(photos.Photos);
            foreach (var photo in photos.Photos)
            {
                Assert.IsNotNull(photo.Scales);
                Assert.Greater(photo.Scales.Length, 0);
            }
        }

        [Test]
        public async Task GetPhotoInfo()
        {
            using var provider = GetFlickrProvider();
            var photo = await provider.GetPhotoInfo("72157718730051451", "51058689738", "1c10361d52");
            Assert.IsNotNull(photo?.Tags);
        }

        [Test]
        public async Task ListCollectionsTest()
        {
            using var provider = GetFlickrProvider();
            var icons = await provider.ListCollectionMosaicIcons();

            Assert.IsNotNull(icons);
            Assert.Greater(0, icons.Count);
        }

        private FlickrProvider GetFlickrProvider()
        {
            var flickrApiKey = Environment.GetEnvironmentVariable("FLICKR_KEY");
            var flickrUserId = Environment.GetEnvironmentVariable("FLICKR_USERID");
            if (flickrApiKey == null || flickrUserId == null)
                throw new Exception("Need to provide FLICKR_KEY and FLICKR_USERID for testing.");
            return new FlickrProvider(flickrApiKey, flickrUserId);
        }
    }
}
