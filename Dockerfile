FROM registry.gitlab.com/weredev/base-containers/dotnet-build:latest AS build-env

WORKDIR /src
ADD . .
WORKDIR "/src/Weredev.UI"
RUN dotnet build "Weredev.UI.csproj" -c Release -o /app/build
RUN dotnet publish "Weredev.UI.csproj" --runtime linux-musl-x64 -c Release -o /app/publish

FROM registry.gitlab.com/weredev/base-containers/dotnet-run:latest AS run-env
COPY --from=build-env /app/publish .

ENV FLICKR_KEY=${FLICKR_KEY}
ENV FLICKR_USERID=${FLICKR_USERID}

EXPOSE 5000

ENTRYPOINT [ "dotnet", "Weredev.UI.dll" ]