using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Weredev.Domain.Interfaces;
using Weredev.Domain.Models.Developer;

namespace Weredev.Providers.SourceControl.GitLab
{
    public sealed class GitLabProvider : ISourceControlProvider, IDisposable
    {
        private HttpClient _httpClient;
        
        public GitLabProvider()
        {
            _httpClient = CreateHttpClient();
        }

        public async Task<string> GetReadmeMarkdown(string repoKey)
        {
            var url = $"{repoKey}/-/raw/master/README.md";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            using var response = await _httpClient.SendAsync(request);

            switch (response.StatusCode)
            {
                case HttpStatusCode.NotFound:
                    return string.Empty;
                case HttpStatusCode.OK:
                    var responseString = await response.Content.ReadAsStringAsync();
                    return responseString;
                default:
                    throw new HttpRequestException($"Error getting Readme.md from GitHub {repoKey}: {response.StatusCode}");
            }
        }

        public async Task<ReleaseNotesDomainModel[]> GetReleaseInfo(string repoKey)
        {
            return await Task.FromResult(new ReleaseNotesDomainModel[0]);
        }

        public void Dispose()
        {
            if (_httpClient != null)
            {
                _httpClient.Dispose();
                _httpClient = null!;
            }
        }

        private HttpClient CreateHttpClient()
        {
            var httpClient = new HttpClient()
            {
                BaseAddress = new Uri("https://gitlab.com/weredev/"),
            };

            httpClient.DefaultRequestHeaders.Add("User-Agent", ".Net Core 3.1.100");
            httpClient.DefaultRequestHeaders.Add("Accept", "*/*");

            return httpClient;
        }

        // private async Task<T> ProcessResponse<T>(HttpResponseMessage response)
        //     where T : class
        // {
        //     var responseContent = await response.Content.ReadAsStringAsync();
        //     if (string.IsNullOrWhiteSpace(responseContent))
        //         return null;
        //     var deserialized = JsonSerializer.Deserialize<T>(responseContent, _jsonSerializerOptions);
        //     return deserialized;
        // }
    }
}
