using System.Text.Json.Serialization;

namespace Weredev.Providers.SourceControl.GitHub
{
    public record GetReadmeMarkdownResponse
    {
        public string? Name { get; init; }

        public string? Path { get; init; }

        public string? Sha { get; init; }

        public int Size { get; init; }

        public string? Url { get; init; }

        [JsonPropertyName("html_url")]
        public string? HtmlUrl { get; init; }

        [JsonPropertyName("git_url")]
        public string? GitUrl { get; init; }

        [JsonPropertyName("download_url")]
        public string? DownloadUrl { get; init; }

        public string? Type { get; init; }

        public string? Content { get; init; }

        public string? Encoding { get; init; }
    }
}
