using System;
using System.Text.Json.Serialization;

namespace Weredev.Providers.SourceControl.GitHub
{
    public record GetReleaseNotesResponse
    {
        [JsonPropertyName("html_url")]
        public string? Url { get; init; }

        [JsonPropertyName("name")]
        public string? Name { get; init; }

        [JsonPropertyName("published_at")]
        public DateTime PublishedAt { get; init; }

        public string? Body { get; init; }

        public Asset[]? Assets { get; init; }

        public record Asset
        {
            public string? Name { get; init; }

            [JsonPropertyName("browser_download_url")]
            public string? DownloadUrl { get; init; }

            [JsonPropertyName("download_count")]
            public int DownloadCount { get; init; }
        }
    }
}
