using AutoMapper;
using Weredev.Domain.Models.Developer;

namespace Weredev.Providers.SourceControl.GitHub
{
    internal class GitHubMapperProfile : Profile
    {
        public GitHubMapperProfile()
        {
            CreateMap<GetReleaseNotesResponse, ReleaseNotesDomainModel>();
            CreateMap<GetReleaseNotesResponse.Asset, ReleaseNotesDomainModel.Asset>();
        }
    }
}
