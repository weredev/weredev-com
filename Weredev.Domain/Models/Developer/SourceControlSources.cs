namespace Weredev.Domain.Models.Developer
{
    public enum SourceControlSources
    {
        Unknown = 0,
        GitHub,
        GitLab,
    }
}
