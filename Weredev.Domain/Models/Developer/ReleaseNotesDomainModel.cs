using System;

namespace Weredev.Domain.Models.Developer
{
    public record ReleaseNotesDomainModel
    {
        public string? Url { get; init; }

        public string? Name { get; init; }

        public DateTime PublishedAt { get; init; }

        public string? Body { get; init; }

        public Asset[] Assets { get; init; } = [];

        public record Asset
        {
            public string? Name { get; init; }

            public string? DownloadUrl { get; init; }

            public int DownloadCount { get; init; }
        }
    }
}
