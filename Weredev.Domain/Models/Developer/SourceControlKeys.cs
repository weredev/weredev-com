namespace Weredev.Domain.Models.Developer
{
    public static class SourceControlKeys
    {
        public const string WeredevKey = "weredev-com";
        public const string ThornlessKey = "thornless";
        public const string Wu10ManKey = "wu10man";
        public const string VsCodeCopyRawKey = "vscode-copyrawvalue";
    }
}
