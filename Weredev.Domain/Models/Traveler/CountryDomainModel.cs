using System.Collections.Generic;

namespace Weredev.Domain.Models.Traveler
{
    public record CountryDomainModel
    {
        public City[] Cities { get; init; } = [];

        public string Key { get; init; } = string.Empty;

        public string? Name { get; init; }

        public record City
        {
            public string[] MosaicIcons { get; init; } = [];

            public string? Description { get; init; }

            public string? Name { get; init; }

            public string Key { get; init; } = string.Empty;
        }
    }
}
