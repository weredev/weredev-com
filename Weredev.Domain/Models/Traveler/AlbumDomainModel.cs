using System;

namespace Weredev.Domain.Models.Traveler
{
    public record AlbumDomainModel
    {
        public string? Description { get; init; }

        public string? IconUrl { get; init; }

        public string? CountryKey { get; init; }

        public string? CountryName { get; init; }

        public string? CityKey { get; init; }

        public string? CityName { get; init; }

        public string? AlbumKey { get; init; }

        public string? AlbumName { get; init; }

        public Photo[] Photos { get; init; } = [];

        public record Photo
        {
            public string? Name { get; init; }

            public string[] Tags { get; init; } = [];

            public PhotoScale[] Scales { get; init; } = [];

            public string? Secret { get; init; }

            public DateTime? DateTaken { get; init; }

            public int Rotation { get; init; }

            public string? Description { get; init; }

            public string? PhotoPageUrl { get; init; }

            public record PhotoScale
            {
                public enum ScaleType
                {
                    Original,
                    Thumbnail,
                    Small,
                    Medium,
                }

                public string? Url { get; init; }

                public int Height { get; init; }

                public int Width { get; init; }

                public ScaleType Scale { get; init; }
            }
        }
    }
}
