using System.Collections.Generic;

namespace Weredev.Domain.Models.Traveler
{
    public record CityDomainModel
    {
        public string? Description { get; init; }

        public string? IconUrl { get; init; }

        public string CountryKey { get; init; } = string.Empty;

        public string? CountryName { get; init; }

        public string CityKey { get; init; } = string.Empty;

        public string? CityName { get; init; }

        public List<Album> Albums { get; init; } = [];

        public record Album
        {
            public string? Description { get; init; }

            public string Id { get; init; } = string.Empty;

            public string? Name { get; init; }

            public string Key { get; init; } = string.Empty;

            public string? IconUrl { get; init; }
        }
    }
}
