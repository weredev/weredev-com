using Weredev.Domain.Extensions;

namespace Weredev.Domain.Models.TravelImageProvider
{
    public record CollectionProviderModel
    {
        private string? _title = null;

        public string Id { get; init; } = string.Empty;

        public string? Description { get; init; }

        public string[] MosaicIcons { get; init; } = [];

        public string CountryName { get; private set; } = string.Empty;

        public string CountryKey { get; private set; } = string.Empty;

        public string CityName { get; private set; } = string.Empty;

        public string CityKey { get; private set; } = string.Empty;

        public Album[] Albums { get; init; } = [];

        public string Title
        {
            get
            {
                return this._title ?? string.Empty;
            }

            set
            {
                _title = value;
                var tempString = value.GetCountryName();
                CountryName = tempString;
                CountryKey = tempString.CreateKey();
                tempString = value.GetCityName();
                CityName = tempString;
                CityKey = tempString.CreateKey();
            }
        }

        public record Album
        {
            private string? _name;

            public string Id { get; init; } = string.Empty;

            public string? Description { get; init; }

            public string Key { get; private set; } = string.Empty;

            public string? Name
            {
                get
                {
                    return _name;
                }

                set
                {
                    _name = value;
                    Key = value?.CreateKey() ?? string.Empty;
                }
            }
        }
    }
}
