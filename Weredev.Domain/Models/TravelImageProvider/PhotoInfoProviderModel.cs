using System;

namespace Weredev.Domain.Models.TravelImageProvider
{
    public record PhotoInfoProviderModel
    {
        public string Id { get; init; } = string.Empty;

        public int Rotation { get; init; }

        public string? Title { get; init; }

        public DateTime DateTaken { get; init; }

        public string[] Tags { get; init; } = [];

        public string? PhotoPageUrl { get; init; }
    }
}
