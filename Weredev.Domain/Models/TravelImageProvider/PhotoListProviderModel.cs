using System;

namespace Weredev.Domain.Models.TravelImageProvider
{
    public record PhotoListProviderModel
    {
        public string Id { get; init; } = string.Empty;

        public string? Primary { get; init; }

        public int Page { get; init; }

        public int PerPage { get; init; }

        public int Pages { get; init; }

        public int Total { get; init; }

        public Photo[] Photos { get; init; } = [];

        public record Photo
        {
            public string Id { get; init; } = string.Empty;

            public string? Name { get; init; }

            public string[] Tags { get; init; } = [];

            public PhotoScale[] Scales { get; init; } = [];

            public string Secret { get; init; } = string.Empty;

            public DateTime? DateTaken { get; init; }

            public string? Title { get; init; }

            public int Rotation { get; init; }

            public string? PhotoPageUrl { get; init; }

            public record PhotoScale
            {
                public enum ScaleType
                {
                    Original,
                    Thumbnail,
                    Small,
                    Medium,
                }

                public string? Url { get; init; }

                public int Height { get; init; }

                public int Width { get; init; }

                public ScaleType Scale { get; init; }
            }
        }
    }
}
