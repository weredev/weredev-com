namespace Weredev.Domain.Models.TravelImageProvider
{
    public record PhotosetProviderModel
    {
        public string Id { get; init; } = string.Empty;

        public string? Name { get; init; }

        public string? Description { get; init; }

        public string? IconUrl { get; init; }

        public int? IconWidth { get; init; }

        public int? IconHeight { get; init; }
    }
}
