using System.Linq;
using System.Text.RegularExpressions;

namespace Weredev.Domain.Extensions
{
    public static partial class StringExtensions
    {
        public static string CreateKey(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return string.Empty;
            var key = NonAlphaNumericRegex().Replace(value.Trim(), "_");
            key = MultipleUnderscoreRegex().Replace(key, "_");
            return key.ToLower();
        }

        public static string GetCountryName(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return string.Empty;
            value = value.Trim();
            if (!value.Contains(','))
                return value;

            var nameParts = value.Split(',');
            var countryName = nameParts[^1].Trim();
            return countryName;
        }

        public static string GetCityName(this string? value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return string.Empty;
            value = value.Trim();
            if (!value.Contains(','))
                return value;

            var nameParts = value.Split(',');
            var cityName = string.Join(", ", nameParts.Take(nameParts.Length - 1));
            return cityName;
        }

        [GeneratedRegex("[^a-zA-Z0-9]")]
        private static partial Regex NonAlphaNumericRegex();
        [GeneratedRegex("_+")]
        private static partial Regex MultipleUnderscoreRegex();
    }
}
