using System;
using System.Threading.Tasks;
using Weredev.Domain.Interfaces;
using Weredev.Domain.Models.Developer;

namespace Weredev.Domain.Services
{
    public class CodeRepoService : ICodeRepoService
    {
        private readonly ISourceControlProviderFactory _scpFactory;
        private readonly IHttpCacheProvider _httpCacheProvider;

        public CodeRepoService(ISourceControlProviderFactory scpFactory, IHttpCacheProvider httpCacheProvider)
        {
            _scpFactory = scpFactory ?? throw new ArgumentNullException(nameof(scpFactory));
            _httpCacheProvider = httpCacheProvider ?? throw new ArgumentNullException(nameof(httpCacheProvider));
        }

        public async Task<string> GetReadmeMarkdown(string repoKey, SourceControlSources source)
        {
            if (string.IsNullOrWhiteSpace(repoKey))
                throw new ArgumentNullException(nameof(repoKey));

            var cacheKey = GetCacheKey(repoKey, source, nameof(GetReadmeMarkdown));

            var markdown = _httpCacheProvider.Get<string>(cacheKey);
            if (markdown == null)
            {
                var provider = _scpFactory.GetSourceControlProvider(source);
                markdown = await provider.GetReadmeMarkdown(repoKey);
                _httpCacheProvider.Set(cacheKey, markdown);
            }

            return markdown;
        }

        public async Task<ReleaseNotesDomainModel[]> GetReleaseHistory(string repoKey, SourceControlSources source)
        {
            if (string.IsNullOrWhiteSpace(repoKey))
                throw new ArgumentNullException(nameof(repoKey));

            var cacheKey = GetCacheKey(repoKey, source, nameof(GetReleaseHistory));

            var releaseInfo = _httpCacheProvider.Get<ReleaseNotesDomainModel[]>(cacheKey);
            if (releaseInfo == null)
            {
                var provider = _scpFactory.GetSourceControlProvider(source);
                releaseInfo = await provider.GetReleaseInfo(repoKey);
                _httpCacheProvider.Set(cacheKey, releaseInfo);
            }

            return releaseInfo;
        }

        private static string GetCacheKey(string repoKey, SourceControlSources source, string method)
        {
            return $"{source}-{repoKey}-{method}";
        }
    }
}
