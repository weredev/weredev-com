using System.Threading.Tasks;
using Weredev.Domain.Models.Developer;

namespace Weredev.Domain.Interfaces
{
    public interface ICodeRepoService
    {
        Task<string> GetReadmeMarkdown(string repoKey, SourceControlSources source);

        Task<ReleaseNotesDomainModel[]> GetReleaseHistory(string repoKey, SourceControlSources source);
    }
}
