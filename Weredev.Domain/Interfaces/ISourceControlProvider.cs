using System.Threading.Tasks;
using Weredev.Domain.Models.Developer;

namespace Weredev.Domain.Interfaces
{
    public interface ISourceControlProvider
    {
        Task<string> GetReadmeMarkdown(string repoKey);

        Task<ReleaseNotesDomainModel[]> GetReleaseInfo(string repoKey);
    }
}
