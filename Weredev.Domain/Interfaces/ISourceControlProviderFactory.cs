using Weredev.Domain.Models.Developer;

namespace Weredev.Domain.Interfaces
{
    public interface ISourceControlProviderFactory
    {
        ISourceControlProvider GetSourceControlProvider(SourceControlSources source);
    }
}
