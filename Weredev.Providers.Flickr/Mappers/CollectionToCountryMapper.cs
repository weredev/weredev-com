using System.Linq;
using Weredev.Providers.Flickr.Models;
using Weredev.Domain.Models.TravelImageProvider;

namespace Weredev.Providers.Flickr.Mappers
{
    public static class CollectionToCountryMapper
    {
        public static CollectionProviderModel ParseTreeResponse(this CollectionsGetTreeResponse.CollectionItem collection)
        {
            var model = new CollectionProviderModel
            {
                Albums = collection.Set?.Select(x => x.ParseToCollectionAlbum()).ToArray() ?? [],
                Description = collection.Description,
                Id = collection.Id,
                Title = collection.Title ?? string.Empty,
            };

            return model;
        }

        private static CollectionProviderModel.Album ParseToCollectionAlbum(this CollectionsGetTreeResponse.Set photoSet)
        {
            return new CollectionProviderModel.Album
            {
                Description = photoSet.Description,
                Id = photoSet.Id,
                Name = photoSet.Title,
            };
        }
    }
}
