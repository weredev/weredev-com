using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Weredev.Providers.Flickr.Mappers;
using Weredev.Domain.Interfaces;
using Weredev.Domain.Models.TravelImageProvider;

namespace Weredev.Providers.Flickr
{
    public sealed class FlickrProvider : IDisposable, ITravelImageProvider
    {
        private const string _RequestBaseUrl = "https://api.flickr.com/services/rest/?";
        private const string _Format = "json";
        private const string _NoJsonCallback = "1";
        private const string _FlickrGetTree = "flickr.collections.getTree";
        private const string _FlickrGetList = "flickr.photosets.getList";
        private const string _FlickrGetPhotos = "flickr.photosets.getPhotos";
        private const string _FlickrGetPhotoInfo = "flickr.photos.getInfo";
        private readonly JsonSerializerOptions _jsonSerializerOptions;
        private readonly string _apiKey;
        private readonly string _userId;
        private HttpClient _httpClient;

        public FlickrProvider(string apiKey, string userId)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
                throw new ArgumentNullException(nameof(apiKey));
            _apiKey = apiKey;
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));
            _userId = userId;
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            _httpClient = new HttpClient();
        }

        public void Dispose()
        {
            if (_httpClient != null)
            {
                _httpClient.Dispose();
                _httpClient = null!;
            }
        }

        public async Task<CollectionProviderModel[]> ListCollections()
        {
            var flickrTree = await ExecuteFlickrRequestAsync<Models.CollectionsGetTreeResponse>(_FlickrGetTree);

            var domainModels = flickrTree.Collections?.Collection?.Select(x => x.ParseTreeResponse())
                                                                .ToArray();

            if (domainModels == null) return [];

            var icons = await ListCollectionMosaicIcons();

            for (int i = 0; i < domainModels.Length; i++)
            {
                var domainModel = domainModels[i];
                var collectionId = domainModel.Id.Split('-').Last();
                if (icons.TryGetValue(collectionId, out string[]? value))
                {
                    domainModels[i] = domainModel with { MosaicIcons = value };
                }
            }

            return domainModels;
        }

        public async Task<Dictionary<string, string[]>> ListCollectionMosaicIcons()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"https://www.flickr.com/photos/{_userId}/collections");
            using var response = await _httpClient.SendAsync(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new HttpRequestException($"Could not get response from Flickr Request.");
            var doc = new HtmlDocument();
            doc.Load(await response.Content.ReadAsStreamAsync());
            var collNodes = doc.DocumentNode.SelectNodes("//div[contains(@class, 'Coll-l')]");

            var collections = collNodes.Select(collNode =>
            {
                var linkNode = collNode.SelectSingleNode(".//a");
                var collectionId = linkNode.GetAttributeValue("href", string.Empty);
                collectionId = collectionId.Trim('/').Split('/').Last();
                var imgNodes = linkNode.SelectNodes("img");
                var imgUrls = imgNodes.Select(imgNode => imgNode.GetAttributeValue("src", string.Empty)).ToArray();

                return new { collectionId, imgUrls };
            }).ToDictionary(x => x.collectionId, x => x.imgUrls);

            return collections;
        }

        public async Task<PhotosetProviderModel[]> ListPhotosets()
        {
            var parameters = new Dictionary<string, string>
            {
                { "primary_photo_extras", "url_s" },
            };

            var photoset = await ExecuteFlickrRequestAsync<Models.PhotosetsGetListResponse>(_FlickrGetList, parameters);
            var models = photoset.ToProviderModel();
            return models;
        }

        public async Task<PhotoListProviderModel> ListPhotos(string photosetId)
        {
            if (string.IsNullOrWhiteSpace(photosetId))
                throw new ArgumentNullException(nameof(photosetId));

            var parameters = new Dictionary<string, string>
            {
                { "photoset_id", photosetId },
                { "extras", "date_taken, o_dims, url_t, url_s, url_m, url_o" },
            };

            var photos = await ExecuteFlickrRequestAsync<Models.PhotosetsGetPhotosResponse>(_FlickrGetPhotos, parameters);
            var model = photos.ToProviderModel();
            return model;
        }

        public async Task<PhotoInfoProviderModel> GetPhotoInfo(string photosetId, string photoId, string secret)
        {
            if (string.IsNullOrWhiteSpace(photoId))
                throw new ArgumentNullException(nameof(photoId));
            if (string.IsNullOrWhiteSpace(secret))
                throw new ArgumentNullException(nameof(secret));

            var parameters = new Dictionary<string, string>
            {
                { "photo_id", photoId },
                { "secret", secret },
            };

            var photoInfo = await ExecuteFlickrRequestAsync<Models.PhotosetsGetInfoResponse>(_FlickrGetPhotoInfo, parameters);
            var model = photoInfo.ToProviderModel(photosetId);
            return model;
        }

        private async Task<T> ExecuteFlickrRequestAsync<T>(string flickrMethod, Dictionary<string, string>? parameters = null)
        {
            var url = CreateFlickrRequestUrl(flickrMethod, parameters);
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            using var response = await _httpClient.SendAsync(request);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new HttpRequestException($"Could not get response from Flickr Request {flickrMethod}: {response.StatusCode}");
            var responseContent = await response.Content.ReadAsStringAsync();
            var deserialized = JsonSerializer.Deserialize<T>(responseContent, _jsonSerializerOptions);
            return deserialized ?? Activator.CreateInstance<T>();
        }

        private string CreateFlickrRequestUrl(string flickrMethod, Dictionary<string, string>? parameters)
        {
            if (string.IsNullOrWhiteSpace(flickrMethod))
                throw new ArgumentNullException(nameof(flickrMethod));

            var queryString = $"{_RequestBaseUrl}&api_key={_apiKey}&user_id={_userId}&format={_Format}&nojsoncallback={_NoJsonCallback}&method={flickrMethod}";

            if (parameters != null && parameters.Count != 0)
            {
                var querySegments = parameters.Select(x => $"{x.Key}={x.Value}");
                var querySuffix = string.Join("&", querySegments);
                queryString += "&" + querySuffix;
            }

            return queryString;
        }
    }
}
