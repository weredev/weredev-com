using System.Text.Json.Serialization;

namespace Weredev.Providers.Flickr.Models
{
    public record PhotosetsGetListResponse
    {
        public SetList? Photosets { get; init; }

        public record SetList
        {
            public int Page { get; init; }

            public int Pages { get; init; }

            public int PerPage { get; init; }

            public int Total { get; init; }

            public SetListItem[]? PhotoSet { get; init; }
        }

        public record SetListItem
        {
            public string? Id { get; init; }

            public int Photos { get; init; }

            public Content? Title { get; init; }

            public Content? Description { get; init; }

            public PrimaryPhotoExtras? Primary_Photo_Extras { get; init; }
        }

        public record Content
        {
            public string? Text { get; init; }
        }

        public record PrimaryPhotoExtras
        {
            [JsonPropertyName("url_s")]
            public string? SmallUrl { get; init; }

            [JsonPropertyName("height_s")]
            public int? SmallHeight { get; init; }

            [JsonPropertyName("width_s")]
            public int? SmallWidth { get; init; }
        }
    }
}
