using System.Text.Json.Serialization;

namespace Weredev.Providers.Flickr.Models
{
    public record PhotosetsGetPhotosResponse
    {
        [JsonPropertyName("photoset")]
        public PhotoSet? Set { get; init; }

        [JsonPropertyName("stat")]
        public string? Status { get; init; }

        public record PhotoSet
        {
            [JsonPropertyName("id")]
            public string? Id { get; init; }

            [JsonPropertyName("primary")]
            public string? PrimaryName { get; init; }

            [JsonPropertyName("page")]
            public int Page { get; init; }

            [JsonPropertyName("perpage")]
            public int PerPage { get; init; }

            [JsonPropertyName("pages")]
            public int TotalPages { get; init; }

            [JsonPropertyName("total")]
            public int TotalPhotos { get; init; }

            [JsonPropertyName("photo")]
            public Photo[]? Photos { get; init; }

            public record Photo
            {
                [JsonPropertyName("id")]
                public string? Id { get; init; }

                [JsonPropertyName("secret")]
                public string? Secret { get; init; }

                [JsonPropertyName("title")]
                public string? Title { get; init; }

                [JsonPropertyName("datetaken")]
                public string? DateTaken { get; init; }

                [JsonPropertyName("url_t")]
                public string? ThumbnailUrl { get; init; }

                [JsonPropertyName("height_t")]
                public int ThumbnailHeight { get; init; }

                [JsonPropertyName("width_t")]
                public int ThumbnailWidth { get; init; }

                [JsonPropertyName("url_s")]
                public string? SmallUrl { get; init; }

                [JsonPropertyName("height_s")]
                public int SmallHeight { get; init; }

                [JsonPropertyName("width_s")]
                public int SmallWidth { get; init; }

                [JsonPropertyName("url_m")]
                public string? MediumUrl { get; init; }

                [JsonPropertyName("height_m")]
                public int MediumHeight { get; init; }

                [JsonPropertyName("width_m")]
                public int MediumWidth { get; init; }

                [JsonPropertyName("url_o")]
                public string? OriginalUrl { get; init; }

                [JsonPropertyName("height_o")]
                public int OriginalHeight { get; init; }

                [JsonPropertyName("width_o")]
                public int OriginalWidth { get; init; }
            }
        }
    }
}
