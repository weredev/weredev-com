namespace Weredev.Providers.Flickr.Models
{
    public record CollectionsGetTreeResponse
    {
        public string? Stat { get; init; }

        public TreeCollection? Collections { get; init; }

        public record TreeCollection
        {
            public CollectionItem[]? Collection { get; init; }
        }

        public record CollectionItem
        {
            public string Id { get; init; } = string.Empty;

            public string? Title { get; init; }

            public string? Description { get; init; }

            public string? IconLarge { get; init; }

            public string? IconSmall { get; init; }

            public Set[]? Set { get; init; }
        }

        public record Set
        {
            public string Id { get; init; } = string.Empty;

            public string? Title { get; init; }

            public string? Description { get; init; }
        }
    }
}
