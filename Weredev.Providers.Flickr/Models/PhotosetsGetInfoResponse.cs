using System.Text.Json.Serialization;

namespace Weredev.Providers.Flickr.Models
{
    public record PhotosetsGetInfoResponse
    {
        public PhotoInfoModel? Photo { get; init; }

        public record PhotoInfoModel
        {
            public string? Id { get; init; }

            public int Rotation { get; init; }

            public OwnerInfo? Owner { get; init; }

            public ContentModel? Title { get; init; }

            public ContentModel? Description { get; init; }

            public DatesModel? Dates { get; init; }

            public TagsModel? Tags { get; init; }

            public UrlContent? Urls { get; init; }

            public record ContentModel
            {
                [JsonPropertyName("_content")]
                public string? Content { get; init; }

                [JsonPropertyName("type")]
                public string? ContentType { get; init; }
            }

            public record DatesModel
            {
                public string? Taken { get; init; }
            }

            public record TagsModel
            {
                [JsonPropertyName("tag")]
                public TagInfoModel[]? Tags { get; init; }
                public record TagInfoModel
                {
                    public string? Raw { get; init; }
                }
            }

            public record OwnerInfo
            {
                public string? NsId { get; init; }
            }

            public record UrlContent
            {
                public ContentModel[]? Url { get; init; }
            }
        }
    }
}
