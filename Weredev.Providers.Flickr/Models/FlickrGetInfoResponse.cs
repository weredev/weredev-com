namespace Weredev.Providers.Flickr.Models
{
    public record FlickrGetInfoResponse
    {
        public string? Stat { get; init; }
    }
}
