using System;
using System.Linq;
using Weredev.Domain.Models.Traveler;

namespace Weredev.UI.Models.WorldTraveler
{
    public record ListPhotosResponse
    {
        public ListPhotosResponse(AlbumDomainModel album)
        {
            AlbumKey = album.AlbumKey;
            AlbumName = album.AlbumName;
            CityKey = album.CityKey;
            CityName = album.CityName;
            CountryKey = album.CountryKey;
            CountryName = album.CountryName;
            Photos = album.Photos.Select(x => new Photo(x)).ToArray();
        }

        public string? CountryKey { get; init; }

        public string? CountryName { get; init; }

        public string? CityKey { get; init; }

        public string? CityName { get; init; }

        public string? AlbumKey { get; init; }

        public string? AlbumName { get; init; }

        public Photo[] Photos { get; }

        public record Photo
        {
            public Photo(AlbumDomainModel.Photo photo)
            {
                DateTaken = photo.DateTaken;
                Name = photo.Name;
                Scales = photo.Scales.Select(x => new PhotoScale(x)).ToArray();
                Secret = photo.Secret;
                Tags = photo.Tags;
                Rotatation = photo.Rotation;
                PhotoPageUrl = photo.PhotoPageUrl;
            }

            public string? Name { get; init; }

            public string[] Tags { get; init; }

            public PhotoScale[] Scales { get; init; }

            public string? Secret { get; init; }

            public DateTime? DateTaken { get; init; }

            public int Rotatation { get; init; }

            public string? PhotoPageUrl { get; init; }

            public string Description
            {
                get
                {
                    var description = string.Empty;
                    if (Tags != null && Tags.Length > 0)
                        description += string.Join(" | ", Tags);

                    if (DateTaken.HasValue)
                        description += " | " + DateTaken.Value.ToString("dd MMM yyyy");

                    return description;
                }
            }

            public record PhotoScale
            {
                public PhotoScale(AlbumDomainModel.Photo.PhotoScale scale)
                {
                    Height = scale.Height;
                    Scale = Enum.Parse<ScaleType>(scale.Scale.ToString());
                    Url = scale.Url;
                    Width = scale.Width;
                }

                public enum ScaleType
                {
                    Original,
                    Thumbnail,
                    Small,
                    Medium,
                }

                public string? Url { get; init; }

                public int Height { get; init; }

                public int Width { get; init; }

                public ScaleType Scale { get; init; }
            }
        }
    }
}
