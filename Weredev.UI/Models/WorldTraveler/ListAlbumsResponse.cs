using System;
using System.Linq;
using Weredev.Domain.Models.Traveler;

namespace Weredev.UI.Models.WorldTraveler
{
    public class ListAlbumsResponse
    {
        public ListAlbumsResponse(CityDomainModel city)
        {
            if (city == null)
                throw new ArgumentNullException(nameof(city));
            if (city.Albums?.Any() != true)
                throw new ArgumentNullException(nameof(city.Albums));

            CityKey = city.CityKey;
            CityName = city.CityName ?? string.Empty;
            CountryKey = city.CountryKey;
            CountryName = city.CountryName ?? string.Empty;
            Albums = city.Albums.Select(x => new Album(city, x)).ToArray();
        }

        public string CountryKey { get; init; }

        public string CountryName { get; init; }

        public string CityKey { get; init; }

        public string CityName { get; init; }

        public Album[] Albums { get; }

        public class Album
        {
            public Album(CityDomainModel city, CityDomainModel.Album album)
            {
                if (album == null)
                    throw new ArgumentNullException(nameof(album));

                AlbumKey = album.Key;
                AlbumName = album.Name ?? string.Empty;
                CityKey = city.CityKey;
                CityName = city.CityName ?? string.Empty;
                CountryKey = city.CountryKey;
                CountryName = city.CountryName ?? string.Empty;
                Description = album.Description ?? string.Empty;
                IconUrl = album.IconUrl ?? string.Empty;
            }

            public string Description { get; }

            public string IconUrl { get; }

            public string CountryKey { get; init; }

            public string CountryName { get; init; }

            public string CityKey { get; init; }

            public string CityName { get; init; }

            public string AlbumKey { get; init; }

            public string AlbumName { get; init; }
        }
    }
}
