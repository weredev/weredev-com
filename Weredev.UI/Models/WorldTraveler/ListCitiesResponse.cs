using System;
using System.Linq;
using Weredev.Domain.Models.Traveler;

namespace Weredev.UI.Models.WorldTraveler
{
    public class ListCitiesResponse
    {
        public ListCitiesResponse(CountryDomainModel country)
        {
            ArgumentNullException.ThrowIfNull(country);
            if (country.Cities.Length == 0)
                throw new ArgumentException("No Cities found in country");

            CountryKey = country.Key;
            CountryName = country.Name ?? string.Empty;
            Cities = country.Cities.Select(x => new CityViewModel(country, x)).ToArray();
        }

        public string CountryKey { get; init; }

        public string CountryName { get; init; }

        public CityViewModel[] Cities { get; }

        public class CityViewModel
        {
            public CityViewModel(CountryDomainModel country, CountryDomainModel.City city)
            {
                if (city == null)
                    throw new ArgumentNullException(nameof(city));
                MosaicIcons = city.MosaicIcons;
                Description = city.Description ?? string.Empty;
                CityName = city.Name ?? string.Empty;
                CityKey = city.Key;
                CountryKey = country.Key;
                CountryName = country.Name ?? string.Empty;
            }

            public string CountryKey { get; init; }

            public string CountryName { get; init; }

            public string CityKey { get; init; }

            public string CityName { get; init; }

            public string[] MosaicIcons { get; }

            public string Description { get; }
        }
    }
}
