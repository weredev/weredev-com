namespace Weredev.UI.Models.Developer
{
    public record ReadmeResponse : DeveloperResponseBase
    {
        public string? HtmlContent { get; init; }
    }
}
