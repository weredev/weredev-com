using System;

namespace Weredev.UI.Models.Developer
{
    public record ReleaseNotesResponse : DeveloperResponseBase
    {
        public Release[] Releases { get; init; } = [];
        
        public record Release
        {
            public string Url { get; init; } = string.Empty;

            public string Name { get; init; } = string.Empty;

            public string SafeTagName => Name.Replace('.', '-');

            public DateTimeOffset PublishedAt { get; init; }

            public string Body { get; init; } = string.Empty;

            public Asset[] Assets { get; init; } = [];

            public record Asset
            {
                public string Name { get; init; } = string.Empty;

                public string DownloadUrl { get; init; } = string.Empty;

                public int DownloadCount { get; init; }
            }
        }
    }
}
