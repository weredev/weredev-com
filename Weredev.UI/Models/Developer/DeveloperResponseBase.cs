using System;

namespace Weredev.UI.Models.Developer
{
    public abstract record DeveloperResponseBase
    {
        public string RepoName { get; init; } = string.Empty;
        
        public bool HasReleaseNotes { get; init; }

        public bool ShowBuyMeACoffee { get; init; } = false;

        public SourceControlTypes SourceControlType { get; init; }

        public string? SourceControlUrl
        {
            get
            {
                return SourceControlType switch
                {
                    SourceControlTypes.GitHub => $"https://github.com/weredev/{RepoName}",
                    SourceControlTypes.GitLab => $"https://gitlab.com/weredev/{RepoName}",
                    _ => null,
                };
            }
        }

        public string? SourceControlIcon
        {
            get
            {
                return SourceControlType switch
                {
                    SourceControlTypes.GitHub => $"/images/developer/github.png",
                    SourceControlTypes.GitLab => $"/images/developer/gitlab.png",
                    _ => null,
                };
            }
        }
    }
}
