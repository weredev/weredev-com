using MarkdownSharp;

namespace Weredev.UI.Models.Developer;

public static class ExtensionMethods
{
    private static readonly Markdown _markdown = new();

    public static string TransformMarkdown(this string markdown)
    {
        if (string.IsNullOrEmpty(markdown))
            return string.Empty;

        var html = _markdown.Transform(markdown);
        html = html.Replace("<h1", "<h4").Replace("h1>", "h4>");
        html = html.Replace("<h2", "<h5").Replace("h2>", "h5>");
        html = html.Replace("<h3", "<h6").Replace("h3>", "h6>");
        return html;
    }
}
