using AutoMapper;
using Weredev.Domain.Models.Developer;

namespace Weredev.UI.Models.Developer
{
    public class DeveloperMapperProfile : Profile
    {
        public DeveloperMapperProfile()
        {
            CreateMap<ReleaseNotesDomainModel, ReleaseNotesResponse.Release>()
                .ForMember(dest => dest.Body, src => src.MapFrom(x => GetBody(x.Body)));
            CreateMap<ReleaseNotesDomainModel.Asset, ReleaseNotesResponse.Release.Asset>();
        }

        private static string GetBody(string? markdown)
        {
            return markdown?.TransformMarkdown() ?? string.Empty;
        }
    }
}
