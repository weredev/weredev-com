namespace Weredev.UI.Models.Developer
{
    public enum SourceControlTypes
    {
        Unknown = 0,
        GitHub,
        GitLab,
    }
}
