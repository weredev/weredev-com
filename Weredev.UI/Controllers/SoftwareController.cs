using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Weredev.Domain.Interfaces;
using Weredev.UI.Models.Developer;
using System.Linq;
using AutoMapper;
using Weredev.UI.Providers;

namespace Weredev.UI.Controllers
{
    public class SoftwareController : BaseController
    {
        private readonly ICodeRepoService _codeRepoService;
        private readonly Mapper _mapper;
        private readonly DeveloperMetaDataProvider _metaData;

        public SoftwareController(ICodeRepoService codeRepoService)
        {
            _codeRepoService = codeRepoService ?? throw new ArgumentNullException(nameof(codeRepoService));
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DeveloperMapperProfile>();
            });
            _mapper = new Mapper(mapperConfig);
            _metaData = new DeveloperMetaDataProvider();
        }

        [HttpGet]
        public IActionResult Index()
        {
            SetTitle("Software");
            SetKeywords(["wu10man", "weredev.com", "thornless", "copy raw value", "vscode"]);
            SetDescription("Showcasing some of the software stuff I've tinkered with writing.");
            return View();
        }

        [HttpGet("[controller]/{repoKey}")]
        public async Task<IActionResult> Readme(string repoKey)
        {
            var metaData = _metaData.GetMetaData(repoKey);
            if (metaData == null)
                return NotFound();

            SetTitle($"{metaData.Title}");
            SetKeywords(metaData.Keywords, "readme");

            var markdown = await _codeRepoService.GetReadmeMarkdown(metaData.SourceControlKey, metaData.SourceControlSource);

            if (string.IsNullOrWhiteSpace(markdown))
                return NotFound();

            var response = await CreateResponse<ReadmeResponse>(metaData);
            response = response with { HtmlContent = markdown.TransformMarkdown() };

            return View(response);
        }

        [HttpGet("[controller]/{repoKey}/releases")]
        public async Task<IActionResult> ReleaseNotes(string repoKey)
        {
            var metaData = _metaData.GetMetaData(repoKey);

            if (metaData == null)
                return NotFound();

            SetTitle($"{metaData.Title}");
            SetKeywords(metaData.Keywords, "Releases", "Release History");

            var response = await CreateResponse<ReleaseNotesResponse>(metaData);

            if (!response.HasReleaseNotes)
                return NotFound();

            var notes = await _codeRepoService.GetReleaseHistory(metaData.SourceControlKey, metaData.SourceControlSource);
            
            response = response with { Releases = notes.Select(x => _mapper.Map<ReleaseNotesResponse.Release>(x)).ToArray() };

            return View(response);
        }

        private async Task<T> CreateResponse<T>(DeveloperMetaData metaData)
            where T : DeveloperResponseBase
        {
            var history = await _codeRepoService.GetReleaseHistory(metaData.SourceControlKey, metaData.SourceControlSource);
            var instance = Activator.CreateInstance<T>() with
            {
                RepoName = metaData.SourceControlKey,
                ShowBuyMeACoffee = metaData.ShowBuyMeACoffee,
                HasReleaseNotes = history.Length > 0,
                SourceControlType = Enum.Parse<SourceControlTypes>(metaData.SourceControlSource.ToString()),
            };
            SetTitle($"{metaData.Title}");
            SetKeywords(metaData.Keywords, "readme");
            if (!string.IsNullOrEmpty(metaData.Description))
                SetDescription(metaData.Description);
            return instance;
        }

        private void SetKeywords(string[] repoWords, params string[] additionalKeywords)
        {
            var keywords = repoWords.Concat(additionalKeywords);

            base.SetKeywords(keywords.ToArray());
        }
    }
}
