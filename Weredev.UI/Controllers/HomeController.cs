using Microsoft.AspNetCore.Mvc;

namespace Weredev.UI.Controllers
{
    public class HomeController : BaseController
    {
        [HttpGet]
        public IActionResult Index()
        {
            SetDescription("Rolling dice, writing code, and wandering the world.");
            SetKeywords("Geek",
                        "Developer",
                        "Travel",
                        "Gaming",
                        "D&D",
                        "Dungeons and Dragons",
                        "Pathfinder",
                        "Board Games",
                        "C#",
                        "DotNet",
                        "DotNetCore",
                        "React",
                        "GitHub",
                        "Wu10Man",
                        "Pictures",
                        "Photos",
                        "Photography",
                        "Flickr");
            return View();
        }
    }
}
