using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;
using System;
using Weredev.Domain.Interfaces;
using Weredev.Domain.Services;
using Weredev.Providers.Flickr;
using Weredev.Providers.SourceControl.GitHub;
using Weredev.Providers.SourceControl.GitLab;
using Weredev.UI.Providers;

namespace Weredev.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var (flickrApiKey, flickrUserId, flickrCacheFolder) = GetFlickrConfig();
            services.AddSingleton<IHttpCacheProvider, HttpCacheProvider>();
            services.AddSingleton<IFileCacheProvider>(new FileCacheProvider(flickrCacheFolder));
            services.AddSingleton<ITravelImageProvider>(new FlickrProvider(flickrApiKey, flickrUserId));
            services.AddSingleton<GitHubProvider>();
            services.AddSingleton<GitLabProvider>();
            services.AddSingleton<ITravelService, TravelService>();
            services.AddSingleton<ICodeRepoService, CodeRepoService>();
            services.AddTransient<IServiceProvider, ServiceProvider>();
            services.AddSingleton<ISourceControlProviderFactory, SourceControlProviderFactory>();

            services.AddControllersWithViews();

            services.Configure<RouteOptions>(options =>
            {
                options.AppendTrailingSlash = true;
                options.LowercaseUrls = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.EnvironmentName == "Development")
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All,
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = "public,max-age=" + durationInSeconds;
                },
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }

        private (string FlickrApiKey, string FlickrUserId, string? FlickrCacheFolder) GetFlickrConfig()
        {
            var flickrApiKey = Configuration.GetValue<string>("FLICKR_KEY");
            if (string.IsNullOrWhiteSpace(flickrApiKey))
                throw new ApplicationException("FLICKR_KEY not set");
            var flickrUserId = Configuration.GetValue<string>("FLICKR_USERID");
            if (string.IsNullOrWhiteSpace(flickrUserId))
                throw new ApplicationException("FLICKR_USERID not set");

            var flickrCacheFolder = Configuration.GetValue<string>("FLICKR_CACHE_FOLDER");
            return (flickrApiKey, flickrUserId, flickrCacheFolder);
        }
    }
}
