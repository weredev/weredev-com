using System;
using System.IO;
using System.Text.Json;
using System.Collections.Generic;
using Weredev.Domain.Interfaces;

namespace Weredev.UI.Providers
{
    public class FileCacheProvider : IFileCacheProvider
    {
        private static readonly HashSet<char> _invalidFileChars = new HashSet<char>(Path.GetInvalidFileNameChars());
        private readonly string _basePath = "./file-cache/";

        public FileCacheProvider(string? basePath)
        {
            if (!string.IsNullOrWhiteSpace(basePath))
                _basePath = basePath;
        }

        public T? Get<T>(string key)
        {
            var fileName = GetPathFromKey(key);
            if (!File.Exists(fileName))
                return default;

            var json = File.ReadAllText(fileName);
            var item = JsonSerializer.Deserialize<FileCacheModelWrapper<T>>(json);
            if (item == null)
                return default;

            if (item.Expiration.HasValue && item.Expiration.Value < DateTime.UtcNow)
                return default;

            return item.Model;
        }

        public void Set<T>(string key, T item)
        {
            WriteFile<T>(key, item, null);
        }

        public void Set<T>(string key, T item, TimeSpan expiration)
        {
            WriteFile<T>(key, item, expiration);
        }

        public void ClearCache()
        {
            Directory.Delete(_basePath, true);
        }

        private void WriteFile<T>(string key, T item, TimeSpan? expiration)
        {
            if (!Directory.Exists(_basePath))
                Directory.CreateDirectory(_basePath);

            var fileName = GetPathFromKey(key);

            var toSave = new FileCacheModelWrapper<T>(item);
            if (expiration.HasValue)
                toSave.Expiration = toSave.CacheDateTime.Add(expiration.Value);
            var json = JsonSerializer.Serialize(toSave);
            File.WriteAllText(fileName, json);
        }

        private string GetPathFromKey(string key)
        {
            var keyChars = key.ToCharArray();
            for (int i = 0; i < keyChars.Length; i++)
            {
                if (_invalidFileChars.Contains(keyChars[i]))
                    keyChars[i] = '_';
            }

            var fileName = new string(keyChars);
            return Path.Combine(_basePath, fileName);
        }
    }
}
