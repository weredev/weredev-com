using System;

namespace Weredev.UI.Providers
{
    public record FileCacheModelWrapper<T>
    {
        public FileCacheModelWrapper(T model)
        {
            Model = model;
            CacheDateTime = DateTime.UtcNow;
        }

        public T Model { get; init; }
        public DateTime CacheDateTime { get; private set; }
        public DateTime? Expiration { get; set; }
    }
}
