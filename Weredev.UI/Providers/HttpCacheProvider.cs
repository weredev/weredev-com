using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections;
using System.Reflection;
using Weredev.Domain.Interfaces;

namespace Weredev.UI.Providers
{
    public class HttpCacheProvider : IHttpCacheProvider
    {
        private readonly IMemoryCache _memoryCache;
        private readonly TimeSpan _expiration = new TimeSpan(1, 0, 0);

        public HttpCacheProvider(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache ?? throw new ArgumentNullException(nameof(memoryCache));
        }

        public T? Get<T>(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));

            var standardizedKey = StandardizeKey(key);

            return _memoryCache.TryGetValue<T>(standardizedKey, out T? t)
                    ? t
                    : default;
        }

        public void Set<T>(string key, T item)
        {
            Set(key, item, _expiration);
        }

        public void Set<T>(string key, T item, TimeSpan expiration)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));

            var standardizedKey = StandardizeKey(key);

            if (item == null)
                _memoryCache.Remove(standardizedKey);
            else
                _memoryCache.Set(standardizedKey, item, expiration);
        }

        public void ClearCache()
        {
            var field = typeof(MemoryCache).GetProperty("EntriesCollection", BindingFlags.NonPublic | BindingFlags.Instance);
            if (field?.GetValue(_memoryCache) is ICollection collection)
            {
                foreach (var item in collection)
                {
                    var methodInfo = item.GetType().GetProperty("Key");
                    var val = methodInfo?.GetValue(item);
                    if (val != null)
                        _memoryCache.Remove(val.ToString()!);
                }
            }
        }

        private string StandardizeKey(string key)
        {
            return key.Trim().ToLower();
        }
    }
}
