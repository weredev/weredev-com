using System;
using Weredev.Domain.Models.Developer;

namespace Weredev.UI.Providers
{
    public record DeveloperMetaData
    {
        public string[] Keywords { get; init; } = [];

        public string? Description { get; init; }

        public string? Title { get; init; }

        public string SourceControlKey { get; init; } = string.Empty;

        public Boolean ShowBuyMeACoffee { get; init; } = false;

        public SourceControlSources SourceControlSource { get; init; }
    }
}
