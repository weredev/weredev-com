using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Weredev.Domain.Interfaces;
using Weredev.Domain.Models.Developer;
using Weredev.Providers.SourceControl.GitHub;
using Weredev.Providers.SourceControl.GitLab;

namespace Weredev.UI.Providers
{
    public class SourceControlProviderFactory : ISourceControlProviderFactory
    {
        private readonly Dictionary<SourceControlSources, ISourceControlProvider> _factoryTypes;
        
        public SourceControlProviderFactory(IServiceProvider serviceProvider)
        {
            var gitHubProvider = serviceProvider.GetRequiredService<GitHubProvider>();
            var gitLabProvider = serviceProvider.GetRequiredService<GitLabProvider>();

            _factoryTypes = new Dictionary<SourceControlSources, ISourceControlProvider>
            {
                { SourceControlSources.GitHub, gitHubProvider },
                { SourceControlSources.GitLab, gitLabProvider },
            };
        }

        public ISourceControlProvider GetSourceControlProvider(SourceControlSources source)
        {
            if (!_factoryTypes.ContainsKey(source))
                throw new ArgumentOutOfRangeException(nameof(source));

            return _factoryTypes[source];
        }
    }
}
