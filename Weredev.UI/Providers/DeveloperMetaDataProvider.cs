using System.Collections.Generic;
using Weredev.Domain.Models.Developer;

namespace Weredev.UI.Providers
{
    public class DeveloperMetaDataProvider
    {
        private readonly Dictionary<string, DeveloperMetaData> _developerMetaData = new Dictionary<string, DeveloperMetaData>();

        public DeveloperMetaDataProvider()
        {
            CreateWeredevComEntry();
            CreateWu10ManEntry();
            CreateThornlessEntry();
            CreateCopyRawEntry();
        }

        public DeveloperMetaData? GetMetaData(string repoUrl)
        {
            var trimmedUrl = repoUrl?.Trim()?.ToLower()?.Replace('.', '-') ?? string.Empty;
            return _developerMetaData.ContainsKey(trimmedUrl)
                    ? _developerMetaData[trimmedUrl]
                    : null;
        }

        private void CreateWeredevComEntry()
        {
            _developerMetaData.Add(
                SourceControlKeys.WeredevKey,
                new DeveloperMetaData
                {
                    Description = "Information and code on the very site that you're looking at.",
                    Keywords = new string[]
                    {
                        "weredev.com",
                        "mvc.net",
                        "dotnet core",
                        "scss",
                        "stylecop",
                        "bootstrap",
                        "github",
                        "azure pipelines",
                        "automapper",
                        "markdownsharp",
                    },
                    SourceControlKey = SourceControlKeys.WeredevKey,
                    SourceControlSource = SourceControlSources.GitLab,
                    Title = "weredev.com",
                });
        }

        private void CreateWu10ManEntry()
        {
            _developerMetaData.Add(
                SourceControlKeys.Wu10ManKey,
                new DeveloperMetaData
                {
                    Description = "Windows 10 has decided that users are no longer smart enough to control their own updates, so I wrote this to grant that control back.",
                    Keywords = new string[]
                    {
                        "wu10man",
                        "Windows 10",
                        "Windows Updates",
                        "Windows Update Manager",
                        "Windows Update Service",
                        "declutter",
                        "WPF",
                    },
                    ShowBuyMeACoffee = true,
                    SourceControlKey = SourceControlKeys.Wu10ManKey,
                    SourceControlSource = SourceControlSources.GitHub,
                    Title = "Wu10Man",
                });
        }

        private void CreateThornlessEntry()
        {
            _developerMetaData.Add(
                SourceControlKeys.ThornlessKey,
                new DeveloperMetaData
                {
                    Description = "Create character names, towns, and such for use with RPGs.",
                    Keywords = new string[]
                    {
                        "thornless",
                        "d&d",
                        "dnd",
                        "dungeons and dragons",
                        "pathfinder",
                        "rpg",
                        "role playing game",
                        "character name",
                        "market name",
                        "town generator",
                    },
                    SourceControlKey = SourceControlKeys.ThornlessKey,
                    SourceControlSource = SourceControlSources.GitLab,
                    Title = "Thornless",
                });
        }

        private void CreateCopyRawEntry()
        {
            _developerMetaData.Add(
                SourceControlKeys.VsCodeCopyRawKey,
                new DeveloperMetaData
                {
                    Description = "VSCode extension to copy the raw value of a string while debugging.",
                    Keywords = new string[]
                    {
                        "vscode",
                        "visual studio code",
                        "copy value",
                        "copy raw value",
                        "extension",
                    },
                    SourceControlKey = SourceControlKeys.VsCodeCopyRawKey,
                    SourceControlSource = SourceControlSources.GitHub,
                    Title = "Copy Raw Value",
                });
        }
    }
}
